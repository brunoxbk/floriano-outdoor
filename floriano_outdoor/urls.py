from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from apps.client import urls as client_urls
from apps.core import urls as core_urls
from apps.agency import urls as agency_urls
from apps.outdoor import urls as outdoors_urls
from apps.rent import urls as rent_urls
from apps.usuarios import urls as users_urls
from apps.reserve import urls as reserve_urls
from apps.cash import urls as cash_urls

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^clientes/', include(client_urls)),
    url(r'^agencias/', include(agency_urls)),
    url(r'^outdoors/', include(outdoors_urls)),
    url(r'^locacao/', include(rent_urls)),
    url(r'^usuarios/', include(users_urls)),
    url(r'^reservas/', include(reserve_urls)),
    url(r'^caixa/', include(cash_urls)),
    url(r'^', include(core_urls)),
]

if settings.DEBUG:
    urlpatterns += static(
        settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
