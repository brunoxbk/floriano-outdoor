from django.db import models
from apps.core.models import SuperClass


class Client(SuperClass):
    name = models.CharField(
        "Nome", max_length=255, null=False, blank=False)
    cpf = models.CharField(
        "CPF", max_length=255, null=False, blank=False)
    rg = models.CharField(
        "RG", max_length=255, null=False, blank=False)
    fone = models.CharField(
        "Telefone", max_length=255, null=True, blank=True)
    email = models.CharField(
        "Email", max_length=255, null=True, blank=True)
    brith = models.DateField('Nascimento', null=True, blank=True)
    observation = models.TextField("Texto", null=True, blank=True)
    cidade = models.CharField(
        "Cidade", max_length=255, null=True, blank=True)
    bairro = models.CharField(
        "Bairro", max_length=255, null=True, blank=True)
    rua = models.CharField(
         "Rua", max_length=255, null=True, blank=True)
    numero = models.CharField(
         "Número", max_length=255, null=True, blank=True)
    cep = models.CharField(
         "CEP", max_length=255, null=True, blank=True)
    creator = models.ForeignKey(
        'auth.User', blank=True, null=True, related_name='creator_client')

    class Meta:
        verbose_name = "Cliente"
        verbose_name_plural = "Clientes"
        ordering = ('name',)
        get_latest_by = '-created_at'

    def __str__(self):
        return self.name
