from django.conf.urls import url
from apps.client import views

app_name = 'client'

urlpatterns = [
    url(r'novo/$',
        views.NewClient.as_view(), name='new'),
    url(r'email/$',
        views.ClientAjaxEmail.as_view(), name='email'),
    url(r'(?P<id>[\w_-]+)/editar/$',
        views.UpdateClient.as_view(), name='edit'),
    url(r'(?P<pk>[\w_-]+)/delete/$',
        views.DeleteClient.as_view(), name='delete'),
    url(r'(?P<pk>[\w_-]+)/detalhe/$',
        views.DetailClient.as_view(), name='detail'),
    url(r'$', views.ListClient.as_view(), name='list'),
]
