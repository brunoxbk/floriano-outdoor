from django import forms
from django.conf import settings


class FormClient(forms.Form):
    name = forms.CharField(
        label='Nome', max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': ''}))
    cpf = forms.CharField(
        label='CPF', max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': ''}))
    rg = forms.CharField(
        label='RG', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
    fone = forms.CharField(
        label='Telefone', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
    email = forms.CharField(
        label='Email', max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': ''}))
    brith = forms.DateField(
        label='Nascimento', input_formats=settings.DATE_INPUT_FORMATS,
        required=False, widget=forms.DateInput(attrs={'class': ''}))
    cidade = forms.CharField(
        label='Cidade', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
    bairro = forms.CharField(
        label='Bairro', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
    rua = forms.CharField(
        label='Rua', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
    numero = forms.CharField(
        label='Número', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
    cep = forms.CharField(
        label='CEP', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
    observation = forms.CharField(
        label='Descrição', required=False,
        widget=forms.Textarea(attrs={'class': 'materialize-textarea'}))


class FormSearchClient(forms.Form):
    term = forms.CharField(
        label='Busca', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
