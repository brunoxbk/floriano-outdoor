from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .forms import FormClient, FormSearchClient
from .models import Client
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.views.generic import View
from django.http import HttpResponse
import json


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class DeleteClient(LoginRequiredMixin, DeleteView):
    model = Client
    success_url = reverse_lazy('client:list')
    template_name = 'client/delete.html'
    success_message = 'Cliente deletado com sucesso'
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(DeleteClient, self).get_context_data(**kwargs)
        context.update(dict(
            title='Deletar Cliente',
            link_list=self.success_url, list="Clientes"))
        return context


class DetailClient(LoginRequiredMixin, DetailView):
    slug_field = 'id'
    model = Client
    context_object_name = 'object'
    template_name = "client/detail.html"

    def get_context_data(self, **kwargs):
        context = super(DetailClient, self).get_context_data(**kwargs)
        context.update(dict(
            title='Detalhe Cliente',
            link_list=reverse_lazy('client:list'), list="Clientes"))
        return context


class NewClient(LoginRequiredMixin, FormView):
    template_name = 'client/form.html'
    form_class = FormClient
    success_url = reverse_lazy('client:list')

    def get_context_data(self, **kwargs):
        context = super(NewClient, self).get_context_data(**kwargs)
        context.update(dict(
            title='Novo Cliente',
            link_list=self.success_url,
            list="Clientes"
            ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        client = Client(
            name=data['name'],
            cpf=data['cpf'],
            email=data['email'],
            fone=data['fone'],
            rg=data['rg'],
            brith=data['brith'],
            observation=data['observation'],
            cidade=data['cidade'],
            bairro=data['bairro'],
            rua=data['rua'],
            numero=data['numero'],
            cep=data['cep'],
        )

        client.save()

        msg = "Cliente adicionado com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(NewClient, self).form_valid(form)


class ListClient(LoginRequiredMixin, ListView):
    template_name = "client/list.html"
    model = Client
    context_object_name = 'clients'
    paginate_by = 10

    def get_queryset(self, **kwargs):
        query = Q(status=True)

        if self.request.GET.get('term', None):
            query.add(
                Q(name__icontains=self.request.GET['term']) |
                Q(cpf__icontains=self.request.GET['term']) |
                Q(observation__icontains=self.request.GET['term']) |
                Q(cidade__icontains=self.request.GET['term']) |
                Q(bairro__icontains=self.request.GET['term']) |
                Q(email__icontains=self.request.GET['term']), Q.AND)

        return Client.objects.filter(query)

    def get_context_data(self, **kwargs):
        context = super(ListClient, self).get_context_data(**kwargs)
        context['form_search'] = FormSearchClient(
            initial=self.request.GET.dict())
        context.update(dict(
            link_list=reverse_lazy('client:list'),
            list="Clientes"
            ))
        return context


class UpdateClient(LoginRequiredMixin, FormView):
    template_name = 'client/form.html'
    form_class = FormClient
    success_url = reverse_lazy('client:list')

    def get_client(self):
        return get_object_or_404(Client, id=self.kwargs.get('id'))

    def get_context_data(self, **kwargs):
        context = super(UpdateClient, self).get_context_data(**kwargs)
        c = self.get_client()
        context['form'] = FormClient(initial=c.__dict__)
        # context.update(dict(msg=self.request.GET.get('msg', None)))
        context.update(dict(
            title='Editar %s' % c.name,
            link_list=self.success_url,
            list="Clientes"
        ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        client = self.get_client()

        client.name = data['name']
        client.email = data['email']
        client.cpf = data['cpf']
        client.fone = data['fone']
        client.rg = data['rg']
        client.brith = data['brith']
        client.observation = data['observation']
        client.cidade = data['cidade']
        client.bairro = data['bairro']
        client.rua = data['rua']
        client.numero = data['numero']
        client.cep = data['cep']

        client.save()

        msg = "Cliente alterado com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(UpdateClient, self).form_valid(form)


class ClientAjaxEmail(View):

    def get(self, request, *args, **kwargs):
        clients = Client.objects.all()
        res = [{'%s - %s' % (c.name, c.email): None} for c in clients]
        return HttpResponse(json.dumps(res), content_type='application/json')
