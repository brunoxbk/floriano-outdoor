from django.conf.urls import url
from apps.usuarios import views

app_name = 'users'

urlpatterns = [
    url(r'novo/$', views.NewUser.as_view(), name='new'),
    url(r'(?P<id>[\w_-]+)/editar/$',
        views.UpdateUser.as_view(), name='edit'),
    url(r'(?P<pk>[\w_-]+)/delete/$',
        views.UserDelete.as_view(), name='delete'),
    url(r'$', views.ListUser.as_view(), name='list'),
]
