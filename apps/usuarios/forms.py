from django import forms
from django.conf import settings
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib.auth.models import Group


class UserChangeForm(forms.Form):
    name = forms.CharField(
        label='Nome', max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': ''}))
    email = forms.CharField(
        label='Email', max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': ''}))
    password = forms.CharField(
        label='Senha', required=False,
        widget=forms.PasswordInput(attrs={'class': ''}))
    confirm_password = forms.CharField(
        label='Confirmar senha', required=False,
        widget=forms.PasswordInput(attrs={'class': ''}))
    # group = forms.ModelChoiceField(
    #     label='Grupo', queryset=Group.objects.all(),
    #     widget=forms.Select(attrs={'class': ''}))

    # is_superuser = forms.BooleanField(label='Admin', required=False)
    is_active = forms.BooleanField(label='Ativo', required=False)
