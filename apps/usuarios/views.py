from django.shortcuts import render
from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.shortcuts import get_object_or_404
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.views.generic import RedirectView
from .forms import UserChangeForm
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect
from django.contrib import messages


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class NewUser(LoginRequiredMixin, FormView):
    template_name = 'usuarios/form.html'
    form_class = UserChangeForm
    success_url = reverse_lazy('users:list')

    def get_context_data(self, **kwargs):
        context = super(NewUser, self).get_context_data(**kwargs)
        # context.update(dict(msg=self.request.GET.get('msg', None)))
        context.update(dict(
            title='Novo Usuário',
            link_list=self.success_url,
            list="Usuários"
            ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        user = User(
            username=data['email'],
            first_name=data['name'],
            email=data['email'],
            is_superuser=False,
            is_active=data['is_active']
        )

        user.save()

        # if 'group' in data:
        #     user.groups.clear()
        #     g = Group.objects.get(name=data['group'])
        #     g.user_set.add(user)

        if data['password'] and data['confirm_password']:
            if data['password'] == data['confirm_password']:
                user.set_password(data['password'])
                user.save()

        return super(NewUser, self).form_valid(form)


class ListUser(LoginRequiredMixin, ListView):
    template_name = "usuarios/list.html"
    model = User
    context_object_name = 'users'
    paginate_by = 10

    def get_queryset(self, **kwargs):
        return User.objects.all().order_by('-date_joined')

    def get_context_data(self, **kwargs):
        context = super(ListUser, self).get_context_data(**kwargs)
        context.update(dict(
            link_list=reverse_lazy('users:list'),
            list="Usuários"
        ))
        return context


class UpdateUser(LoginRequiredMixin, FormView):
    template_name = 'usuarios/form.html'
    form_class = UserChangeForm
    success_url = reverse_lazy('users:list')

    def get_user(self):
        return get_object_or_404(User, id=self.kwargs.get('id'))

    def get_context_data(self, **kwargs):
        context = super(UpdateUser, self).get_context_data(**kwargs)
        c = self.get_user()

        i = {
            'username': c.email, 'name': c.first_name,
            'email': c.email, 'is_active': c.is_active
        }

        context['form'] = UserChangeForm(initial=i)

        context.update(dict(
            title='Editar %s' % c.first_name,
            link_list=self.success_url,
            list="Usuários"
        ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data
        user = self.get_user()

        user.username = data['email']
        user.first_name = data['name']
        user.email = data['email']
        # user.is_superuser = data['is_superuser']
        user.is_active = data['is_active']

        user.save()

        if data['password'] and data['confirm_password']:
            if data['password'] == data['confirm_password']:
                user.set_password(data['password'])
                user.save()

        return super(UpdateUser, self).form_valid(form)

    def form_invalid(self, form):
        for key in form.errors:
            print(key)
        return super(UpdateUser, self).form_invalid(form)


class UserDelete(LoginRequiredMixin, DeleteView):
    model = User
    success_url = reverse_lazy('users:list')
    template_name = 'usuarios/delete.html'
    success_message = 'Usuário deletado com sucesso'
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(UserDelete, self).get_context_data(**kwargs)
        context.update(dict(
            title='Deletar Usuário',
            link_list=self.success_url, list="Usuários"))
        return context
