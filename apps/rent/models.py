from django.db import models
from apps.core.models import SuperClass


class Location(SuperClass):

    client = models.ForeignKey(
        'client.Client', blank=True, null=True, related_name='client_location')
    agency = models.ForeignKey(
        'agency.Agency', blank=True, null=True, related_name='agency_location')

    outdoor = models.ForeignKey(
        'outdoor.Outdoor',
        blank=True, null=True, related_name='outdoor_location')

    start = models.DateField('Início', null=True, blank=True)
    end = models.DateField('Fim', null=True, blank=True)

    creator = models.ForeignKey(
        'auth.User', blank=True, null=True, related_name='creator_location')

    class Meta:
        verbose_name = "Locação"
        verbose_name_plural = "Locações"
        ordering = ('start',)
        get_latest_by = '-created_at'

    def __str__(self):
        return "Locacao outdoor %d" % self.outdoor.id
