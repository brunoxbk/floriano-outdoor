from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .forms import FormLocation, FormSearchLocation
from apps.rent.models import Location
from apps.cash.models import Movement
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from datetime import datetime
from django.contrib import messages


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class DeleteLocation(LoginRequiredMixin, DeleteView):
    model = Location
    success_url = reverse_lazy('rent:list')
    template_name = 'rent/delete.html'
    success_message = 'Locação deletada com sucesso'
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(DeleteLocation, self).get_context_data(**kwargs)
        context.update(dict(
            title='Deletar Locação',
            link_list=self.success_url, list="Locações"))
        return context


class DetailLocation(LoginRequiredMixin, DetailView):
    slug_field = 'id'
    model = Location
    context_object_name = 'object'
    template_name = "rent/detail.html"

    def get_context_data(self, **kwargs):
        context = super(DetailLocation, self).get_context_data(**kwargs)
        context.update(dict(
            title='Detalhe Locação',
            link_list=reverse_lazy('rent:list'), list="Locações"))
        return context


class NewLocation(LoginRequiredMixin, FormView):
    template_name = 'rent/form.html'
    form_class = FormLocation
    success_url = reverse_lazy('rent:list')

    def get_context_data(self, **kwargs):
        context = super(NewLocation, self).get_context_data(**kwargs)
        # context.update(dict(msg=self.request.GET.get('msg', None)))
        context.update(dict(
            title='Locar Outdoor',
            link_list=self.success_url, list="Locações"))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        location = Location(
            client=data['client'],
            agency=data['agency'],
            start=data['start'],
            outdoor=data['outdoor'],
            end=data['end']
        )

        location.save()

        if data['cash']:
            movement = Movement(
                history=location.client.name,
                kind='RE',
                date=data['date'],
                value=data['value'],
                close=False,
                observation=data['observation'],
            )

            movement.save()

        if data['alert']:
            outdoors = [{'outdoor': location.outdoor, 'price': None}, ]
            ctx = {
                'outdoors': outdoors,
                'title': 'Locação de outdoor',
                'text': '''
                    Olá %s, segue foto do outdoor que você acacou de locar
                    no periodo de %s à %s.
                ''' % (
                        location.client.name,
                        location.start.strftime('%d/%m/%Y'),
                        location.end.strftime('%d/%m/%Y')
                    )
            }
            msg_html = render_to_string(
                'core/email.html', ctx)
            msg_plain = strip_tags(msg_html)
            send_mail(
                "Locação de outdoor",
                msg_plain,
                settings.EMAIL_HOST_USER,
                [location.client.email],
                html_message=msg_html,
            )

        msg = "Locação cadastrada com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(NewLocation, self).form_valid(form)

    def form_invalid(self, form):
        for key in form.errors:
            if key != '__all__':
                msg = '%s - %s' % (form[key].label, form.errors[key].as_text())
                messages.add_message(self.request, messages.WARNING, msg)
        return super(NewLocation, self).form_invalid(form)


class ListLocation(LoginRequiredMixin, ListView):
    template_name = "rent/list.html"
    model = Location
    context_object_name = 'locations'
    paginate_by = 15

    def get_queryset(self, **kwargs):
        query = Q(status=True)

        if self.request.GET.get('start', None):
            start = datetime.strptime(self.request.GET['start'], '%d/%m/%Y')
            query.add(Q(start__gte=start), Q.AND)

        if self.request.GET.get('end', None):
            end = datetime.strptime(self.request.GET['end'], '%d/%m/%Y')
            query.add(Q(end__lte=end), Q.AND)

        if self.request.GET.get('term', None):
            query.add(
                Q(outdoor__code__icontains=self.request.GET['term']) |
                Q(agency__name__icontains=self.request.GET['term']) |
                Q(client__name__icontains=self.request.GET['term']), Q.AND)

        return Location.objects.filter(query).order_by('-end')

    def get_context_data(self, **kwargs):
        context = super(ListLocation, self).get_context_data(**kwargs)
        context['form_search'] = FormSearchLocation(
            initial=self.request.GET.dict())
        context.update(dict(
            link_list=reverse_lazy('rent:list'),
            list="Locações"
            ))
        return context


class UpdateLocation(LoginRequiredMixin, FormView):
    template_name = 'rent/form.html'
    form_class = FormLocation
    success_url = reverse_lazy('rent:list')

    def get_location(self):
        return get_object_or_404(Location, id=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        context = super(UpdateLocation, self).get_context_data(**kwargs)
        l = self.get_location()

        context['form'] = FormLocation(initial=l.__dict__)

        context['form'].fields["location"].initial = l.id

        if l.agency:
            context['form'].fields["agency"].initial = l.agency.id

        if l.outdoor:
            context['form'].fields["outdoor"].initial = l.outdoor.id

        if l.client:
            context['form'].fields["client"].initial = l.client.id

        # context.update(dict(msg=self.request.GET.get('msg', None)))
        context.update(dict(
            title='Editar locação %s - %s' % (l.client.name, l.outdoor.code),
            link_list=self.success_url,
            list="Clientes"
        ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        location = self.get_location()

        location.client = data['client']
        location.agency = data['agency']
        location.start = data['start']
        location.outdoor = data['outdoor']
        location.end = data['end']

        location.save()

        msg = "Locação alterada com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(UpdateLocation, self).form_valid(form)

    def form_invalid(self, form):
        for key in form.errors:
            if key != '__all__':
                msg = '%s - %s' % (form[key].label, form.errors[key].as_text())
                messages.add_message(self.request, messages.WARNING, msg)
        return super(UpdateLocation, self).form_invalid(form)
