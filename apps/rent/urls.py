from django.conf.urls import url
from apps.rent import views

app_name = 'rent'

urlpatterns = [
    url(r'novo/$',
        views.NewLocation.as_view(), name='new'),
    url(r'(?P<pk>[\w_-]+)/editar/$',
        views.UpdateLocation.as_view(), name='edit'),
    url(r'(?P<pk>[\w_-]+)/delete/$',
        views.DeleteLocation.as_view(), name='delete'),
    url(r'(?P<pk>[\w_-]+)/detalhe/$',
        views.DetailLocation.as_view(), name='detail'),
    url(r'$',
        views.ListLocation.as_view(), name='list'),
]
