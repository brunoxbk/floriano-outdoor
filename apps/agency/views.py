from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .forms import FormAgency, FormSearchAgency
from .models import Agency
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.contrib import messages


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class DeleteAgency(LoginRequiredMixin, DeleteView):
    model = Agency
    success_url = reverse_lazy('agency:list')
    template_name = 'agency/delete.html'
    success_message = 'Agência deletada com sucesso'
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(DeleteAgency, self).get_context_data(**kwargs)
        context.update(dict(
            title='Deletar Agência',
            link_list=self.success_url, list="Agências"))
        return context


class DetailAgency(LoginRequiredMixin, DetailView):
    slug_field = 'id'
    model = Agency
    context_object_name = 'object'
    template_name = "agency/detail.html"

    def get_context_data(self, **kwargs):
        context = super(DetailAgency, self).get_context_data(**kwargs)
        context.update(dict(
            title='Detalhe Agência',
            link_list=reverse_lazy('agency:list'), list="Agências"))
        return context


class NewAgency(LoginRequiredMixin, FormView):
    template_name = 'agency/form.html'
    form_class = FormAgency
    success_url = reverse_lazy('agency:list')

    def get_context_data(self, **kwargs):
        context = super(NewAgency, self).get_context_data(**kwargs)
        context.update(dict(
            title='Nova Agência', link_list=self.success_url, list="Agências"
            ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        agency = Agency(
            name=data['name'],
            email=data['email'],
            fone=data['fone'],
            observation=data['observation'],
            address=data['address'],
            cidade=data['cidade'],
        )

        agency.save()

        msg = "Ageência cadastrada com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(NewAgency, self).form_valid(form)


class ListAgency(LoginRequiredMixin, ListView):
    template_name = "agency/list.html"
    model = Agency
    context_object_name = 'agencys'
    paginate_by = 10

    def get_queryset(self, **kwargs):
        query = Q(status=True)

        if self.request.GET.get('term', None):
            query.add(
                Q(name__icontains=self.request.GET['term']) |
                Q(email__icontains=self.request.GET['term']) |
                Q(cidade__icontains=self.request.GET['term']), Q.AND)

        return Agency.objects.filter(query)

    def get_context_data(self, **kwargs):
        context = super(ListAgency, self).get_context_data(**kwargs)
        context['form_search'] = FormSearchAgency(
            initial=self.request.GET.dict())
        context.update(dict(
            link_list=reverse_lazy('agency:list'), list="Agências"))
        return context


class UpdateAgency(LoginRequiredMixin, FormView):
    template_name = 'agency/form.html'
    form_class = FormAgency
    success_url = reverse_lazy('agency:list')

    def get_client(self):
        return get_object_or_404(Agency, id=self.kwargs.get('id'))

    def get_context_data(self, **kwargs):
        context = super(UpdateAgency, self).get_context_data(**kwargs)
        c = self.get_client()
        context['form'] = FormAgency(initial=c.__dict__)
        # context.update(dict(msg=self.request.GET.get('msg', None)))
        context.update(dict(
            title='Editar %s' % c.name,
            link_list=self.success_url,
            list="Agências"
        ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        agency = self.get_client()

        agency.name = data['name']
        agency.email = data['email']
        agency.fone = data['fone']
        agency.address = data['address']
        agency.observation = data['observation']
        agency.cidade = data['cidade']

        agency.save()

        msg = "Agência alterada com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(UpdateAgency, self).form_valid(form)
