from django import forms


class FormAgency(forms.Form):
    name = forms.CharField(
        label='Nome', max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': ''}))
    fone = forms.CharField(
        label='Telefone', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
    email = forms.CharField(
        label='Email', max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': ''}))
    cidade = forms.CharField(
        label='Cidade', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
    address = forms.CharField(
        label='Endereço', required=False,
        widget=forms.Textarea(attrs={'class': 'materialize-textarea'}))
    observation = forms.CharField(
        label='Descrição', required=False,
        widget=forms.Textarea(attrs={'class': 'materialize-textarea'}))


class FormSearchAgency(forms.Form):
    term = forms.CharField(
        label='Busca', max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': ''}))
