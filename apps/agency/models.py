from django.db import models
from apps.core.models import SuperClass
from django.contrib.auth.models import User


class Agency(SuperClass):
    name = models.CharField(
        "Nome", max_length=255, null=False, blank=False)
    fone = models.CharField(
        "Telefone", max_length=255, null=True, blank=True)
    email = models.CharField(
        "Email", max_length=255, null=True, blank=True)
    observation = models.TextField("Texto", null=True, blank=True)
    address = models.TextField("Endereco", null=True, blank=True)
    cidade = models.CharField(
        "Cidade", max_length=255, null=True, blank=True)
    creator = models.ForeignKey(
        User, blank=True, null=True, related_name='creator_agency')

    class Meta:
        verbose_name = "Agência"
        verbose_name_plural = "Agências"
        ordering = ('name',)
        get_latest_by = '-created_at'

    def __str__(self):
        return self.name
