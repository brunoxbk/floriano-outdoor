from django.conf.urls import url
from apps.agency import views

app_name = 'agency'


urlpatterns = [
    url(r'novo/$',
        views.NewAgency.as_view(), name='new'),
    url(r'(?P<id>[\w_-]+)/editar/$',
        views.UpdateAgency.as_view(), name='edit'),
    url(r'(?P<pk>[\w_-]+)/delete/$',
        views.DeleteAgency.as_view(), name='delete'),
    url(r'(?P<pk>[\w_-]+)/detalhe/$',
        views.DetailAgency.as_view(), name='detail'),
    url(r'$', views.ListAgency.as_view(), name='list'),
]
