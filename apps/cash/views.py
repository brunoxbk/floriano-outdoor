from django.views.generic.edit import FormView
from django.views.generic.list import ListView
from apps.cash.forms import FormMovement, SearchFormMovement
from apps.cash.models import Movement
from django.views.generic.detail import DetailView
from django.shortcuts import get_object_or_404
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from django.db.models import Q
from django.db.models import Sum
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.forms.models import model_to_dict
import datetime
from django.contrib import messages


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class NewMovement(LoginRequiredMixin, FormView):
    template_name = 'cash/form.html'
    form_class = FormMovement
    success_url = '/caixa/?received=N'

    def get_context_data(self, **kwargs):
        context = super(NewMovement, self).get_context_data(**kwargs)
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        movement = Movement(
            history=data['history'],
            kind=data['kind'],
            date=data['date'],
            value=data['value'],
            close=data['close'],
            observation=data['observation'],
        )

        movement.save()
        return super(NewMovement, self).form_valid(form)

    def form_invalid(self, form):
        for key in form.errors:
            msg = '%s - %s' % (form[key].label, form.errors[key].as_text())
            print(msg)
            messages.add_message(self.request, messages.WARNING, msg)
        return super(NewMovement, self).form_invalid(form)


class MovementDetail(LoginRequiredMixin, DetailView):
    slug_field = 'id'
    model = Movement
    context_object_name = 'movement'
    template_name = "cash/detail.html"

    def get_context_data(self, **kwargs):
        context = super(MovementDetail, self).get_context_data(**kwargs)
        return context


class ListMovemet(LoginRequiredMixin, ListView):
    template_name = "cash/list.html"
    model = Movement
    context_object_name = 'movements'
    query = False

    def get_queryset(self, **kwargs):
        query = Q(status=True)

        if self.request.GET.get('history', None):
            h = self.request.GET['history']
            query.add(Q(history__icontains=h), Q.AND)

        if self.request.GET.get('start_date', None):
            s = datetime.datetime.strptime(
                self.request.GET['start_date'], "%d/%m/%Y").date()
            query.add(Q(date__gte=s), Q.AND)

        if self.request.GET.get('end_date', None):
            e = datetime.datetime.strptime(
                self.request.GET['end_date'], "%d/%m/%Y").date()
            query.add(Q(date__lte=e), Q.AND)

        if self.request.GET.get('received', None):
            if self.request.GET['received'] == 'S':
                query.add(Q(close=True), Q.AND)
            elif self.request.GET['received'] == 'N':
                query.add(Q(close=False), Q.AND)

        if self.request.GET.get('history', None):
            self.query = True
            query.add(Q(history__icontains=self.request.GET['history']), Q.AND)

        if self.request.GET.get('kind', None) and self.request.GET['kind']:
            self.query = True
            query.add(Q(kind=self.request.GET['kind']), Q.AND)

        return Movement.objects.filter(query).order_by('date')

    def get_context_data(self, **kwargs):
        context = super(ListMovemet, self).get_context_data(**kwargs)

        # qs = self.get_queryset()
        # vs = qs.aggregate(Sum('value'))
        re = Q()
        # re.add(Q(close=True), Q.AND)
        ex = Q()

        if self.request.GET.get('history', None):
            h = self.request.GET['history']
            re.add(Q(history__icontains=h), Q.AND)
            ex.add(Q(history__icontains=h), Q.AND)

        if self.request.GET.get('received', None):
            if self.request.GET['received'] == 'S':
                re.add(Q(close=True), Q.AND)
                ex.add(Q(close=True), Q.AND)
            elif self.request.GET['received'] == 'N':
                re.add(Q(close=False), Q.AND)
                ex.add(Q(close=False), Q.AND)

        if self.request.GET.get('start_date', None):
            s = datetime.datetime.strptime(
                self.request.GET['start_date'], "%d/%m/%Y").date()
            re.add(Q(date__gte=s), Q.AND)
            ex.add(Q(date__gte=s), Q.AND)

        if self.request.GET.get('end_date', None):
            e = datetime.datetime.strptime(
                self.request.GET['end_date'], "%d/%m/%Y").date()
            ex.add(Q(date__lte=e), Q.AND)
            re.add(Q(date__lte=e), Q.AND)

        re.add(Q(kind='RE'), Q.AND)
        ts_re = Movement.objects.filter(re).aggregate(Sum('value'))

        ex.add(Q(kind='EX'), Q.AND)
        ts_ex = Movement.objects.filter(ex).aggregate(Sum('value'))

        print(ts_re['value__sum'], ts_ex['value__sum'])

        re_val, ex_val = 0, 0

        if ts_re['value__sum'] is not None:
            re_val = ts_re['value__sum']

        if ts_ex['value__sum'] is not None:
            ex_val = ts_ex['value__sum']

        # print(re_val, ex_val)

        balance = re_val - ex_val
        # print('balance')
        # print(balance)

        context.update(dict(
            re_val=re_val,
            ex_val=ex_val,
            query=self.query,
            balance=balance,
            v2=balance,
            link_list=reverse_lazy('cash:list'),
            list="Movimentações",
            form_search=SearchFormMovement(initial=self.request.GET.dict()),
        ))
        return context


class UpdateMovement(LoginRequiredMixin, FormView):
    template_name = 'cash/form.html'
    form_class = FormMovement
    success_url = '/caixa/?received=N'

    def get_movement(self):
        return get_object_or_404(Movement, id=self.kwargs.get('id'))

    def get_context_data(self, **kwargs):
        context = super(UpdateMovement, self).get_context_data(**kwargs)
        c = self.get_movement()
        context['form'] = FormMovement(initial=model_to_dict(c))
        context.update(dict(
            title='Editar %s' % c.history,
            link_list=self.success_url,
            list="Movimentações"
        ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        movement = self.get_movement()
        movement.history = data['history']
        movement.kind = data['kind']
        movement.date = data['date']
        movement.observation = data['observation']
        movement.value = data['value']
        movement.close = data['close']

        movement.save()

        return super(UpdateMovement, self).form_valid(form)

    def form_invalid(self, form):
        for key in form.errors:
            msg = '%s - %s' % (key, form.errors[key].as_text())
            messages.add_message(self.request, messages.WARNING, msg)
        return super(UpdateMovement, self).form_invalid(form)


class MovementDelete(LoginRequiredMixin, DeleteView):
    model = Movement
    success_url = '/caixa/?received=N'
    template_name = 'cash/delete.html'
    success_message = 'Movimentação deletada com sucesso'
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(MovementDelete, self).get_context_data(**kwargs)
        context.update(dict(
            title='Deletar Movimentação',
            link_list=self.success_url, list="Movimentações"))
        return context
