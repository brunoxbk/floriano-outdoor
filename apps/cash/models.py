from django.db import models
from apps.core.models import SuperClass
from datetime import date


class Movement(SuperClass):
    KIND_CHOICE = (
        ('RE', 'Receita'),
        ('EX', 'Despesa'),
    )
    history = models.CharField(
        "Histórico", max_length=255, null=False, blank=False)
    close = models.BooleanField("Recebida", default=False)
    kind = models.CharField('Natureza', max_length=2, choices=KIND_CHOICE)
    date = models.DateField('Data', null=True, blank=True)
    observation = models.TextField("Texto", null=True, blank=True)
    value = models.DecimalField(
        "Valor", decimal_places=2, max_digits=8,
        null=False, blank=False, default=0)
    creator = models.ForeignKey(
        'auth.User', blank=True, null=True, related_name='creator_movement')

    @property
    def is_past_due(self):
        return date.today() > self.date and self.close

    @property
    def class_table(self):
        if self.kind == 'EX':
            return 'red accent-1'
        elif date.today() > self.date and not self.close:
            return ' yellow accent-1'
        elif date.today() >= self.date and self.close and self.kind == 'RE':
            return 'green accent-1'
        elif date.today() <= self.date and self.close and self.kind == 'RE':
            return 'green accent-1'
        elif date.today() <= self.date and \
                not self.close and self.kind == 'RE':
            return 'blue accent-1'

    class Meta:
        verbose_name = "Movimento"
        verbose_name_plural = "Movimentos"

    def __str__(self):
        return self.history
