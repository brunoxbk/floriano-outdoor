from django import forms
from apps.cash.models import Movement
from django.conf import settings


class FormMovement(forms.Form):
    history = forms.CharField(
        label='Histórico', required=True,
        widget=forms.TextInput(attrs={'class': ''}))
    kind = forms.ChoiceField(
        label='Natureza', required=True,
        choices=Movement.KIND_CHOICE,
        widget=forms.Select(attrs={'class': ''}))
    close = forms.BooleanField(
        label='Recebido', required=False,
        widget=forms.CheckboxInput(attrs={'class': ''}))

    date = forms.DateField(
        label='Data', required=True,
        input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.DateInput(
            attrs={'class': 'datepicker', 'readonly': True}))
    value = forms.DecimalField(
        label='Valor',
        widget=forms.NumberInput(attrs={'class': ''}))
    observation = forms.CharField(
        label='Descrição', required=False,
        widget=forms.Textarea(attrs={'class': 'materialize-textarea'}))


class SearchFormMovement(forms.Form):
    history = forms.CharField(
        label='Histórico', required=False,
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    kind = forms.ChoiceField(
        label='Natureza', required=False,
        choices=(('', '-----'),) + Movement.KIND_CHOICE,
        widget=forms.Select(attrs={'class': 'form-control'}))
    start_date = forms.CharField(
        label='Data Inicial', required=False,
        widget=forms.TextInput(attrs={'class': 'datepicker'}))
    end_date = forms.CharField(
        label='Data Final', required=False,
        widget=forms.TextInput(attrs={'class': 'datepicker'}))
    received = forms.ChoiceField(
        label='Recebido', required=False,
        choices=(('', '-----'), ('N', 'Não'), ('S', 'Sim')),
        widget=forms.Select(attrs={'class': 'form-control'}))
