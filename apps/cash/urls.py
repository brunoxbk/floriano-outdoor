from django.conf.urls import url
from apps.cash import views

app_name = 'cash'

urlpatterns = [
    url(r'novo/$',
        views.NewMovement.as_view(), name='new'),
    url(r'(?P<id>[\w_-]+)/editar/$',
        views.UpdateMovement.as_view(), name='edit'),
    url(r'(?P<pk>[\w_-]+)/delete/$',
        views.MovementDelete.as_view(), name='delete'),
    # url(r'(?P<pk>[\w_-]+)/detalhe/$',
    #     views.DetailMovement.as_view(), name='detail'),
    url(r'$', views.ListMovemet.as_view(), name='list'),
]
