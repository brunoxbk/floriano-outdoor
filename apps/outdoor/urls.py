from django.conf.urls import url
from apps.outdoor import views

app_name = 'outdoor'

urlpatterns = [
    url(r'novo/$',
        views.NewOutdoor.as_view(), name='new'),
    url(r'(?P<id>[\w_-]+)/editar/$',
        views.UpdateOutdoor.as_view(), name='edit'),
    url(r'(?P<pk>[\w_-]+)/delete/$',
        views.DeleteOutdoor.as_view(), name='delete'),
    url(r'(?P<pk>[\w_-]+)/email/add/$',
        views.OutdoorEmailAdd.as_view(), name='email'),
    url(r'(?P<pk>[\w_-]+)/email/clear/one/$',
        views.OutdoorEmailClearOne.as_view(), name='email-clear-one'),

    url(r'email/clear/all/$',
        views.OutdoorEmailClearAll.as_view(), name='email-clear'),

    url(r'email/send/$',
        views.OutdoorEmailSend.as_view(), name='email-send'),

    url(r'(?P<pk>[\w_-]+)/locar/$',
        views.LocaOutdoor.as_view(), name='locar'),

    url(r'(?P<pk>[\w_-]+)/convert/$',
        views.Convert.as_view(), name='convert'),


    url(r'(?P<pk>[\w_-]+)/reserva/$',
        views.ReserveOutdoor.as_view(), name='reserva'),

    url(r'(?P<pk>[\w_-]+)/detalhe/$',
        views.DetailOutdoor.as_view(), name='detail'),
    url(r'$', views.ListOutdoor.as_view(), name='list'),
]
