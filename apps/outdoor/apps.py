from django.apps import AppConfig


class OutdoorConfig(AppConfig):
    name = 'outdoor'
