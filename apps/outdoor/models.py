from django.db import models
from apps.core.models import SuperClass
from django.contrib.auth.models import User
from versatileimagefield.fields import VersatileImageField
from django.db.models import Q
from datetime import datetime


class Outdoor(SuperClass):
    """
        Código
        Agência (a ser escolhida entre as cadastradas)
        Tamanho Largura x Altura.
        Cidade
        Bairro
        Rua
        Ponto de Referencia
        Foto da placa
    """
    code = models.CharField(
        "Código", max_length=255, null=False, blank=False)
    cidade = models.CharField(
        "Cidade", max_length=255, null=False, blank=False)
    bairro = models.CharField(
        "Bairro", max_length=255, null=False, blank=False)
    rua = models.CharField(
        "Rua", max_length=255, null=True, blank=True)
    referencia = models.CharField(
        "Referência", max_length=255, null=True, blank=True)
    image = VersatileImageField(
        upload_to='outdoors/', default='outdoors/no-img.jpg')
    # imagem = VersatileImageField(
    #     'Imagem', upload_to='uploads/notice/%Y/%m/', null=True)
    agency = models.ForeignKey(
        'agency.Agency', blank=True, null=True, related_name='agency_outdoor')
    creator = models.ForeignKey(
        User, blank=True, null=True, related_name='creator_outdoor')

    class Meta:
        verbose_name = "Outdoor"
        verbose_name_plural = "Outdoors"
        ordering = ('code',)
        get_latest_by = '-created_at'

    @property
    def is_rent(self):
        start = datetime.now().date()
        query = Q(status=True)
        query.add(Q(end__gte=start), Q.AND)
        return self.outdoor_location.filter(query).exists()

    @property
    def is_reserved(self):
        start = datetime.now().date()
        query = Q(status=True)
        query.add(Q(end__gte=start), Q.AND)
        # query.add(Q(end__lte=stop), Q.AND)
        return self.outdoor_reserve.filter(query).exists()

    def __str__(self):
        return "%s - %s - %s" % (self.code, self.cidade, self.bairro)
