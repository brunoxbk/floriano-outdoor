from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .forms import FormOutdoor, FormLocation, FormSearchOutdoor, FormReserve
from .models import Outdoor
from apps.reserve.models import Reserve
from apps.cash.models import Movement
from apps.rent.models import Location
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import RedirectView
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from django.contrib import messages
import datetime


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + datetime.timedelta(n)


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class DeleteOutdoor(LoginRequiredMixin, DeleteView):
    model = Outdoor
    success_url = reverse_lazy('outdoor:list')
    template_name = 'outdoor/delete.html'
    success_message = 'Outdoor deletado com sucesso'
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(DeleteOutdoor, self).get_context_data(**kwargs)
        context.update(dict(
            title='Deletar Outdoor',
            link_list=self.success_url, list="Outdoors"))
        return context


class DetailOutdoor(LoginRequiredMixin, DetailView):
    slug_field = 'id'
    model = Outdoor
    context_object_name = 'object'
    template_name = "outdoor/detail.html"

    def get_context_data(self, **kwargs):
        context = super(DetailOutdoor, self).get_context_data(**kwargs)

        page_r = self.request.GET.get('page_r', 1)
        page_l = self.request.GET.get('page_l', 1)

        paginator_r = Paginator(
            self.object.outdoor_reserve.all().order_by('-start'), 5)
        paginator_l = Paginator(
            self.object.outdoor_location.all().order_by('-start'), 5)

        try:
            reserves = paginator_r.page(page_r)
        except PageNotAnInteger:
            reserves = paginator_r.page(1)
        except EmptyPage:
            reserves = paginator_r.page(paginator_r.num_pages)

        try:
            locations = paginator_l.page(page_l)
        except PageNotAnInteger:
            locations = paginator_l.page(1)
        except EmptyPage:
            locations = paginator_l.page(paginator_l.num_pages)

        context.update(dict(
            title='Detalhe Outdoor',
            paginator_l=paginator_l,
            paginator_r=paginator_r,
            reserves=reserves,
            locations=locations,
            link_list=reverse_lazy('outdoor:list'), list="Outdoors"))
        return context


class NewOutdoor(LoginRequiredMixin, FormView):
    template_name = 'outdoor/form.html'
    form_class = FormOutdoor
    success_url = reverse_lazy('outdoor:list')

    def get_context_data(self, **kwargs):
        context = super(NewOutdoor, self).get_context_data(**kwargs)
        context.update(dict(
            title='Novo Outdoor',
            link_list=self.success_url,
            list="Outdoors"
            ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        outdoor = Outdoor(
            code=data['code'],
            cidade=data['cidade'],
            bairro=data['bairro'],
            rua=data['rua'],
            referencia=data['referencia'],
            image=data['image'],
        )

        outdoor.save()

        msg = "Outdoor adicionado com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(NewOutdoor, self).form_valid(form)


class LocaOutdoor(LoginRequiredMixin, FormView):
    template_name = 'outdoor/form.html'
    form_class = FormLocation
    success_url = reverse_lazy('outdoor:list')

    def get_outdoor(self):
        return get_object_or_404(Outdoor, id=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        context = super(LocaOutdoor, self).get_context_data(**kwargs)
        context['form'] = FormLocation(
            initial={'outdoor': int(self.kwargs.get('pk'))})
        context.update(dict(
            title='Locar outdoor',
            link_list=self.success_url,
            list="Outdoors"
            ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        location = Location(
            client=data['client'],
            agency=data['agency'],
            start=data['start'],
            outdoor=self.get_outdoor(),
            end=data['end']
        )

        location.save()

        if data['cash']:
            movement = Movement(
                history=location.client.name,
                kind='RE',
                date=data['date'],
                value=data['value'],
                close=False,
                observation=data['observation'],
            )

            movement.save()

        if data['alert']:
            outdoors = [{'outdoor': location.outdoor, 'price': None}, ]
            ctx = {
                'outdoors': outdoors,
                'title': 'Locação de outdoor',
                'text': '''
                    Olá %s, segue foto do outdoor que você acacou de locar
                    no periodo de %s à %s.
                ''' % (
                        location.client.name,
                        location.start.strftime('%d/%m/%Y'),
                        location.end.strftime('%d/%m/%Y')
                    )
            }
            msg_html = render_to_string(
                'core/email.html', ctx)
            msg_plain = strip_tags(msg_html)
            send_mail(
                "Locação de outdoor",
                msg_plain,
                settings.EMAIL_HOST_USER,
                [location.client.email],
                html_message=msg_html,
            )

        msg = "Locação cadastrada com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(LocaOutdoor, self).form_valid(form)

    def form_invalid(self, form):
        for key in form.errors:
            if key != '__all__':
                msg = '%s - %s' % (form[key].label, form.errors[key].as_text())
                messages.add_message(self.request, messages.WARNING, msg)
        return super(LocaOutdoor, self).form_invalid(form)


class ReserveOutdoor(LoginRequiredMixin, FormView):
    template_name = 'outdoor/form.html'
    form_class = FormReserve
    success_url = reverse_lazy('outdoor:list')

    def get_outdoor(self):
        return get_object_or_404(Outdoor, id=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        context = super(ReserveOutdoor, self).get_context_data(**kwargs)
        context['form'] = FormReserve(
            initial={'outdoor': int(self.kwargs.get('pk'))})
        context.update(dict(
            title='Reserva outdoor',
            link_list=self.success_url,
            list="Outdoors"
            ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        reserve = Reserve(
            client=data['client'],
            agency=data['agency'],
            start=data['start_date'],
            outdoor=self.get_outdoor(),
            end=data['end_date']
        )

        reserve.save()

        if data['alert']:
            outdoors = [{'outdoor': reserve.outdoor, 'price': None}, ]
            ctx = {
                'outdoors': outdoors,
                'title': 'Reserva de outdoor',
                'text': '''
                    Olá %s, segue foto do outdoor que você acacou de reservar
                    no periodo de %s à %s.
                ''' % (
                        reserve.client.name,
                        reserve.start.strftime('%d/%m/%Y'),
                        reserve.end.strftime('%d/%m/%Y')
                    )
            }
            msg_html = render_to_string(
                'core/email.html', ctx)
            msg_plain = strip_tags(msg_html)
            send_mail(
                "Reserva de outdoor",
                msg_plain,
                settings.EMAIL_HOST_USER,
                [reserve.client.email],
                html_message=msg_html,
            )

        msg = "Reserva cadastrada com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(ReserveOutdoor, self).form_valid(form)

    def form_invalid(self, form):
        for key in form.errors:
            if key != '__all__':
                msg = '%s - %s' % (form[key].label, form.errors[key].as_text())
                messages.add_message(self.request, messages.WARNING, msg)
        return super(ReserveOutdoor, self).form_invalid(form)


class ListOutdoor(LoginRequiredMixin, ListView):
    template_name = "outdoor/list.html"
    model = Outdoor
    context_object_name = 'outdoors'
    paginate_by = 10

    def get_queryset(self, **kwargs):
        query = Q(status=True)

        if self.request.GET.get('term', None):
            query.add(
                Q(code__icontains=self.request.GET['term']) |
                Q(cidade__icontains=self.request.GET['term']) |
                Q(bairro__icontains=self.request.GET['term']) |
                Q(rua__icontains=self.request.GET['term']) |
                Q(referencia__icontains=self.request.GET['term']), Q.AND)

        outdoors = Outdoor.objects.filter(query)
        filtered = []

        status = self.request.GET.get('status', '0')

        if status == '3':
            filtered = [x for x in outdoors if x.is_rent]
        if status == '2':
            filtered = [x for x in outdoors if x.is_reserved]
        if status == '1':
            filtered = [x for x in outdoors if not x.is_rent]
        if status == '0':
            filtered = outdoors

        return filtered

    def get_context_data(self, **kwargs):
        context = super(ListOutdoor, self).get_context_data(**kwargs)
        context['form_search'] = FormSearchOutdoor(
            initial=self.request.GET.dict())
        context.update(dict(
            link_list=reverse_lazy('outdoor:list'), list="Outdoors"))

        if 'email' not in self.request.session:
            self.request.session['email'] = []

        return context


class UpdateOutdoor(LoginRequiredMixin, FormView):
    template_name = 'outdoor/form.html'
    form_class = FormOutdoor
    success_url = reverse_lazy('outdoor:list')

    def get_outdoor(self):
        return get_object_or_404(Outdoor, id=self.kwargs.get('id'))

    def get_context_data(self, **kwargs):
        context = super(UpdateOutdoor, self).get_context_data(**kwargs)
        o = self.get_outdoor()
        context['form'] = FormOutdoor(initial=o.__dict__)
        if o.image:
            context['form'].fields["image"].initial = o.image.url
        # context.update(dict(msg=self.request.GET.get('msg', None)))
        context.update(dict(
            title='Editar %s' % o.code,
            link_list=self.success_url,
            list="Outdoors"
        ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        outdoor = self.get_outdoor()

        outdoor.code = data['code']
        outdoor.cidade = data['cidade']
        outdoor.bairro = data['bairro']
        outdoor.referencia = data['referencia']
        outdoor.rua = data['rua']

        if 'image' in data and data['image'] is not None:
            outdoor.image = data['image']

        outdoor.save()

        msg = "Outdoor alterado com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(UpdateOutdoor, self).form_valid(form)


class OutdoorEmailAdd(LoginRequiredMixin, RedirectView):
    success_url = reverse_lazy('outdoor:list')
    url = reverse_lazy('outdoor:list')

    def get(self, request, *args, **kwargs):

        pk = self.kwargs.get('pk', None)
        outdoor = Outdoor.objects.get(pk=pk)
        if 'email' not in self.request.session:
            self.request.session['email'] = []
        else:
            exist = False
            for index, item in enumerate(self.request.session['email']):
                if item['id'] == int(self.kwargs.get('pk')):
                    exist = True
                    break

            if not exist:
                self.request.session['email'] = \
                    self.request.session['email'] + [{
                        'id': outdoor.id, 'code': outdoor.code,
                        'image': outdoor.image.url}, ]
        return super(OutdoorEmailAdd, self).get(request, *args, **kwargs)


class OutdoorEmailClearAll(LoginRequiredMixin, RedirectView):
    success_url = reverse_lazy('outdoor:list')
    url = reverse_lazy('outdoor:list')

    def get(self, request, *args, **kwargs):
        self.request.session['email'] = []
        return super(OutdoorEmailClearAll, self).get(request, *args, **kwargs)


class OutdoorEmailClearOne(LoginRequiredMixin, RedirectView):
    success_url = reverse_lazy('outdoor:list')
    url = reverse_lazy('outdoor:list')

    def get(self, request, *args, **kwargs):
        print('--------------------\n\nentrou')
        for index, item in enumerate(self.request.session['email']):
            if item['id'] == int(self.kwargs.get('pk')):
                outs = self.request.session['email']
                del outs[index]
                self.request.session['email'] = outs
                break
        return super(OutdoorEmailClearOne, self).get(request, *args, **kwargs)


class OutdoorEmailSend(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('outdoor:list')

    def post(self, request, *args, **kwargs):
        # client_id = self.kwargs.get('client_id', None)
        # client = get_object_or_404(Client, id=client_id)
        try:
            print(request.POST)
            ids = request.POST['ids'].split(',')
            ids = list(filter(None, ids))
            print(ids)
            outdoors = list()
            for out in Outdoor.objects.filter(id__in=ids):
                price = None
                key = 'price-%d' % out.id
                if key in request.POST and request.POST[key] != '':
                    price = request.POST[key]
                outdoors.append({'outdoor': out, 'price': price})

            print(outdoors)
            msg_html = render_to_string(
                'core/email.html', {
                    'title': request.POST['titulo'],
                    'text': request.POST['texto'],
                    'outdoors': outdoors
                })
            msg_plain = strip_tags(msg_html)
            send_mail(
                request.POST['titulo'],
                msg_plain,
                settings.EMAIL_HOST_USER,
                [request.POST['email']],
                html_message=msg_html,
            )
            msg = "Email enviado com sucesso!"
            messages.add_message(self.request, messages.SUCCESS, msg)
        except Exception as e:
            msg = e
            messages.add_message(self.request, messages.ERROR, msg)
        return super(OutdoorEmailSend, self).get(request, *args, **kwargs)


class Convert(LoginRequiredMixin, RedirectView):
    success_url = reverse_lazy('rent:list')
    url = reverse_lazy('rent:list')

    def get(self, request, *args, **kwargs):
        res = self.kwargs.get('pk', None)
        r = Reserve.objects.get(id=res)
        loc = Location(
            client=r.client,
            agency=r.agency,
            start=r.start,
            outdoor=r.outdoor,
            end=r.end
        )
        loc.save()
        r.delete()

        msg = "Reserva convertida em locação!"
        messages.add_message(self.request, messages.SUCCESS, msg)
        return super(Convert, self).get(request, *args, **kwargs)
