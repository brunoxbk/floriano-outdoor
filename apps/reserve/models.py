from django.db import models
from apps.core.models import SuperClass


class Reserve(SuperClass):

    client = models.ForeignKey(
        'client.Client', blank=True, null=True, related_name='client_reserve')
    agency = models.ForeignKey(
        'agency.Agency', blank=True, null=True, related_name='agency_reserve')

    outdoor = models.ForeignKey(
        'outdoor.Outdoor',
        blank=True, null=True, related_name='outdoor_reserve')

    start = models.DateField('Início', null=True, blank=True)
    end = models.DateField('Fim', null=True, blank=True)

    creator = models.ForeignKey(
        'auth.User', blank=True, null=True, related_name='creator_reserve')

    class Meta:
        verbose_name = "Reserva"
        verbose_name_plural = "Reservas"
        ordering = ('start',)
        get_latest_by = '-created_at'
