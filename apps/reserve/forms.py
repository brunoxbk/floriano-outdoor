from django import forms
from apps.client.models import Client
from apps.agency.models import Agency
from apps.outdoor.models import Outdoor
from django.conf import settings
from django.db.models import Q
import datetime


def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + datetime.timedelta(n)


class FormReserve(forms.Form):
    outdoor = forms.ModelChoiceField(
        label='Outdoor', queryset=Outdoor.objects.all(),  required=False,
        widget=forms.Select(attrs={'class': ''}))
    client = forms.ModelChoiceField(
        label='Cliente', queryset=Client.objects.all(),  required=False,
        widget=forms.Select(attrs={'class': ''}))
    agency = forms.ModelChoiceField(
        label='Agencia', queryset=Agency.objects.all(),  required=False,
        widget=forms.Select(attrs={'class': ''}))
    start = forms.DateField(
        label='Inicio', input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.DateInput(attrs={'class': 'datepicker'}))
    end = forms.DateField(
        label='Fim', input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.DateInput(attrs={'class': 'datepicker'}))
    alert = forms.BooleanField(
        label='Aleta de Email', required=False, initial=False,
        widget=forms.CheckboxInput(attrs={'class': ''}))
    reserve = forms.IntegerField(
        label='', widget=forms.HiddenInput(), initial='', required=False)

    def clean(self):
        cleaned_data = super(FormReserve, self).clean()
        today = datetime.date.today()
        data_start = cleaned_data['start']
        data_end = cleaned_data['end']
        outdoor = cleaned_data['outdoor']
        reserve_id = cleaned_data['reserve']

        if data_start >= data_end:
            self.add_error(
                'start', "Data de início menor que data final")
        # elif today <= data_start:
        #     self.add_error(
        #         'start', "Data de início menor que data atual")
        else:
            ex = Q()
            if reserve_id:
                ex.add(Q(id=reserve_id), Q.AND)
            for check_date in daterange(data_start, data_end):
                query = Q(status=True)
                query.add(
                    Q(start__lte=check_date) & Q(end__gte=check_date), Q.AND)
                if outdoor.outdoor_location.filter(query).exists():
                    self.add_error('end', "Outdoor locado nessa data")
                    break

            for check_date in daterange(data_start, data_end):
                query = Q(status=True)
                query.add(
                    Q(start__lte=check_date) & Q(end__gte=check_date), Q.AND)
                if outdoor.outdoor_reserve.filter(query).exclude(ex).exists():
                    self.add_error('end', "Outdoor reservado nessa data")
                    break


class FormSearchReserve(forms.Form):
    term = forms.CharField(
        label='Busca', max_length=80, required=False,
        widget=forms.TextInput(attrs={'class': ''}))
    start = forms.DateField(
        label='Inicio', input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.DateInput(attrs={'class': 'datepicker'}))
    end = forms.DateField(
        label='Fim', input_formats=settings.DATE_INPUT_FORMATS,
        widget=forms.DateInput(attrs={'class': 'datepicker'}))
