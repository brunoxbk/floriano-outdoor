from django.conf.urls import url
from apps.reserve import views

app_name = 'reserve'

urlpatterns = [
    url(r'novo/$', views.NewReserve.as_view(), name='new'),
    url(r'(?P<pk>[\w_-]+)/editar/$',
        views.UpdateReserve.as_view(), name='edit'),
    url(r'(?P<pk>[\w_-]+)/delete/$',
        views.DeleteReserve.as_view(), name='delete'),
    url(r'(?P<pk>[\w_-]+)/detalhe/$',
        views.DetailReserve.as_view(), name='detail'),
    url(r'$', views.ListReserve.as_view(), name='list'),
]
