from django.views.generic.edit import FormView
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from .forms import FormReserve, FormSearchReserve
from apps.reserve.models import Reserve
from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.generic.edit import DeleteView
from django.core.urlresolvers import reverse_lazy
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.conf import settings
from datetime import datetime
from django.contrib import messages


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class DeleteReserve(LoginRequiredMixin, DeleteView):
    model = Reserve
    success_url = reverse_lazy('reserve:list')
    template_name = 'reserve/delete.html'
    success_message = 'Reserva deletada com sucesso'
    slug_field = 'pk'

    def get_context_data(self, **kwargs):
        context = super(DeleteReserve, self).get_context_data(**kwargs)
        context.update(dict(
            title='Deletar Reserva',
            link_list=self.success_url, list="Reservas"))
        return context


class DetailReserve(LoginRequiredMixin, DetailView):
    slug_field = 'id'
    model = Reserve
    context_object_name = 'object'
    template_name = "reserve/detail.html"

    def get_context_data(self, **kwargs):
        context = super(DetailReserve, self).get_context_data(**kwargs)
        context.update(dict(
            title='Detalhe Reserva',
            link_list=reverse_lazy('reserve:list'), list="Reservas"))
        return context


class NewReserve(LoginRequiredMixin, FormView):
    template_name = 'reserve/form.html'
    form_class = FormReserve
    success_url = reverse_lazy('reserve:list')

    def get_context_data(self, **kwargs):
        context = super(NewReserve, self).get_context_data(**kwargs)
        # context.update(dict(msg=self.request.GET.get('msg', None)))
        context.update(dict(
            title='Reserva de Outdoor',
            link_list=self.success_url,
            list="Reservas"))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        reserve = Reserve(
            client=data['client'],
            agency=data['agency'],
            start=data['start'],
            outdoor=data['outdoor'],
            end=data['end']
        )

        reserve.save()

        if data['alert']:
            outdoors = [{'outdoor': reserve.outdoor, 'price': None}, ]
            ctx = {
                'outdoors': outdoors,
                'title': 'Reserva de outdoor',
                'text': '''
                    Olá %s, segue foto do outdoor que você acacou de reservar
                    no Periodo de %s à %s.
                ''' % (
                    reserve.client.name,
                    reserve.start.strftime('%d/%m/%Y'),
                    reserve.end.strftime('%d/%m/%Y')
                    )
            }
            msg_html = render_to_string(
                'core/email.html', ctx)
            msg_plain = strip_tags(msg_html)
            send_mail(
                "Reserva de outdoor",
                msg_plain,
                settings.EMAIL_HOST_USER,
                [reserve.client.email],
                html_message=msg_html,
            )

        msg = "Reserva cadastrada com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(NewReserve, self).form_valid(form)

    def form_invalid(self, form):
        for key in form.errors:
            msg = '%s - %s' % (form[key].label, form.errors[key].as_text())
            messages.add_message(self.request, messages.WARNING, msg)
        return super(NewReserve, self).form_invalid(form)


class ListReserve(LoginRequiredMixin, ListView):
    template_name = "reserve/list.html"
    model = Reserve
    context_object_name = 'reserves'
    paginate_by = 15

    def get_queryset(self, **kwargs):
        query = Q(status=True)

        if self.request.GET.get('start', None):
            start = datetime.strptime(self.request.GET['start'], '%d/%m/%Y')
            query.add(Q(start__gte=start), Q.AND)

        if self.request.GET.get('end', None):
            end = datetime.strptime(self.request.GET['end'], '%d/%m/%Y')
            query.add(Q(end__lte=end), Q.AND)

        if self.request.GET.get('term', None):
            query.add(
                Q(outdoor__code__icontains=self.request.GET['term']) |
                Q(agency__name__icontains=self.request.GET['term']) |
                Q(client__name__icontains=self.request.GET['term']), Q.AND)

        return Reserve.objects.filter(query).order_by('-end')

    def get_context_data(self, **kwargs):
        context = super(ListReserve, self).get_context_data(**kwargs)
        context['form_search'] = FormSearchReserve(
            initial=self.request.GET.dict())
        context.update(dict(
            link_list=reverse_lazy('reserve:list'), list="Reservas"))
        return context


class UpdateReserve(LoginRequiredMixin, FormView):
    template_name = 'reserve/form.html'
    form_class = FormReserve
    success_url = reverse_lazy('reserve:list')

    def get_reserve(self):
        return get_object_or_404(Reserve, id=self.kwargs.get('pk'))

    def get_context_data(self, **kwargs):
        context = super(UpdateReserve, self).get_context_data(**kwargs)
        l = self.get_reserve()

        context['form'] = FormReserve(initial=l.__dict__)

        context['form'].fields["reserve"].initial = l.id

        if l.agency:
            context['form'].fields["agency"].initial = l.agency.id

        if l.outdoor:
            context['form'].fields["outdoor"].initial = l.outdoor.id

        if l.client:
            context['form'].fields["client"].initial = l.client.id

        # context.update(dict(msg=self.request.GET.get('msg', None)))
        context.update(dict(
            title='Editar reserva %s - %s' % (l.client.name, l.outdoor.code),
            link_list=self.success_url,
            list="Reservas"
        ))
        return context

    def form_valid(self, form):
        data = form.cleaned_data

        reserve = self.get_reserve()

        reserve.client = data['client']
        reserve.agency = data['agency']
        reserve.start = data['start']
        reserve.outdoor = data['outdoor']
        reserve.end = data['end']

        reserve.save()

        msg = "Reserva alterada com sucesso!"
        messages.add_message(self.request, messages.SUCCESS, msg)

        return super(UpdateReserve, self).form_valid(form)

    def form_invalid(self, form):
        for key in form.errors:
            msg = '%s - %s' % (form[key].label, form.errors[key].as_text())
            messages.add_message(self.request, messages.WARNING, msg)
        return super(UpdateReserve, self).form_invalid(form)
