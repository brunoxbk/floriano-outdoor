from django.db import models


class SuperClass(models.Model):
    class Meta:
        abstract = True
        ordering = ('-created_at',)
        get_latest_by = '-created_at'

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    status = models.BooleanField("Status", default=True)
