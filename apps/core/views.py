from django.views.generic.base import TemplateView
from django.contrib.auth import authenticate, login, logout
from django.utils.decorators import method_decorator
from django.views.generic import FormView, RedirectView
from django.contrib.auth.decorators import login_required
from .forms import FormCustomLogin
from django.http import HttpResponseRedirect
from django.contrib import messages
from apps.rent.models import Location
from apps.reserve.models import Reserve
from apps.cash.models import Movement
import datetime


class LoginRequiredMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self)\
            .dispatch(request, *args, **kwargs)


class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "core/home.html"

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        today = datetime.date.today()

        movements = Movement.objects.\
            filter(date__month=today.month, kind='RE', close=False).\
            order_by('date')
        locations = Location.objects.\
            filter(end__month=today.month).order_by('-end')
        reserves = Reserve.objects.\
            filter(start__month=today.month).order_by('-start')
        context.update(dict(
            movements=movements,
            locations=locations,
            reserves=reserves
            ))
        return context


class LoginView(FormView):
    success_url = '/'
    # form_class = AuthenticationForm
    form_class = FormCustomLogin
    # redirect_field_name = REDIRECT_FIELD_NAME
    template_name = "core/login.html"

    def form_valid(self, form):
        data = form.cleaned_data

        user = authenticate(
            self.request,
            username=data['username'],
            password=data['password'])

        if user is not None:
            if user.is_active:
                login(self.request, user)
                return HttpResponseRedirect('/')
            else:
                msg = 'Conta inativa'
                messages.add_message(self.request, messages.WARNING, msg)
                return HttpResponseRedirect('/login/')
        else:
            msg = 'Usuário não encontrado'
            messages.add_message(self.request, messages.WARNING, msg)
            return HttpResponseRedirect('/login/')
        return super(LoginView, self).form_valid(form)


class LogoutView(LoginRequiredMixin, RedirectView):
    url = '/login/'
    permanent = True

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)
