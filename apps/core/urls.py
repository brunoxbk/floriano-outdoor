from django.conf.urls import url
from apps.core import views

app_name = 'core'

urlpatterns = [
    url(r'^$', views.HomeView.as_view(), name='dash'),
    url(r'^login/$', views.LoginView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
]
