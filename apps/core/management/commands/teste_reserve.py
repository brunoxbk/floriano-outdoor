from django.core.management.base import BaseCommand, CommandError
from apps.outdoor.models import Outdoor
from django.conf import settings
from django.db.models import Q
import datetime


class Command(BaseCommand):

    def daterange(self, start_date, end_date):
        for n in range(int((end_date - start_date).days)):
            yield start_date + datetime.timedelta(n)

    def handle(self, *args, **options):
        datetime.date(2014, 4, 7)
        data_start = datetime.date(2017, 10, 25)
        data_end = datetime.date(2017, 11, 10)
        outdoor_id = 1

        outdoor = Outdoor.objects.get(id=outdoor_id)

        if data_end <= data_start:
            print('data errada')
        else:
            print('data ok')

            # r = outdoor.outdoor_reserve.filter(query)
            # print(r)

            for check_date in self.daterange(data_start, data_end):
                print('---------')
                print(check_date)
                query = Q(status=True)
                query.add(
                    Q(start__lte=check_date) &
                    Q(end__gte=check_date), Q.AND)
                l = outdoor.outdoor_reserve.filter(query).exists()
                print(l)
