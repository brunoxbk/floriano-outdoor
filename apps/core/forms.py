from django import forms


class FormCustomLogin(forms.Form):
    username = forms.CharField(
        label='Username', max_length=80, required=True,
        widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(
        label='Senha', max_length=80, required=True,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}))
